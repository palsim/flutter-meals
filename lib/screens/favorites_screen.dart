import 'package:flutter/material.dart';
import '../widgets/meal_item.dart';
import '../models/meal.dart';

class FavoritesScreen extends StatelessWidget {
  final List<Meal> favoriteMeals;

  FavoritesScreen(this.favoriteMeals);

  @override
  Widget build(BuildContext context) {
    //
    //BUG:
    //
    //qd user vai a 1 meal a partir da daqui (lista de favoritos)
    // e retira a meal dos favoritos e faz back, a meal nao sai da listagem
    //de favoritos (isto sera resolvido no proximo capitulo onde explica o
    //stage managment). pra ja, existem workarounds q dariam para fazer mas
    //nao sao recomendados (assim como ter toda a gestao da app no file main.dart
    //traz problemas pq qq alteracao implica rebuild de toda a app -> problema
    //igualmente resolvido com a introducao de state managment)
    //
    if (favoriteMeals.isEmpty) {
      return Center(child: Text("You have no favorites yet!"));
    } else {
      return ListView.builder(
        itemBuilder: (ctx, index) {
          return MealItem(
            id: favoriteMeals[index].id,
            title: favoriteMeals[index].title,
            imageUrl: favoriteMeals[index].imageUrl,
            duration: favoriteMeals[index].duration,
            complexity: favoriteMeals[index].complexity,
            affordability: favoriteMeals[index].affordability,
          );
        },
        itemCount: favoriteMeals.length,
      );
    }
  }
}
